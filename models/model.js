const mongoose = require('mongoose')
const DB_URL = 'mongodb://localhost:27017/eastnsun'
mongoose.connect(DB_URL, function(err, db) {
    if (err) {
        console.log('Unable to connect to the Mongodb. Please start the server. Error:', err);
    } else {
        console.log('Connected to Mongodb successfully!');
    }
});

const models = {
	user:{
		'user':{type:String, 'require':true},
		'pwd':{type:String, 'require':true},
		'type':{'type':String, 'require':true},
		//头像
		'avatar':{'type':String},
		// 个人简介或者职位简介
		'desc':{'type':String},
		// 职位名
		'title':{'type':String},
		// 如果你是boss 还有两个字段
		'company':{'type':String},
		'money':{'type':String}
	},
	account:{
        'uid':{type:String, 'require':true},
        'openid':{type:String, 'require':true},
        'login_token':{type:String, 'require':true},
        'login_type':{type:String, 'require':true},
        'type':{type:String, 'require':true},
        'active':{type:Number, 'require':true}
    },
    company:{
        'uid':{type:String, 'require':true},
        'company_uid':{type:String, 'require':true},
        'company_name':{type:String, 'require':true},
        'company_len':{type:String, 'require':true},
        'company_abn':{type:String, 'require':true},
        'company_acn':{type:String, 'require':true},
        'company_phone':{type:String, 'require':true},
        'company_fax':{type:String, 'require':true},
        'company_email':{type:String, 'require':true},
        'company_addr':{type:String, 'require':true},
        'company_addr_post':{type:String, 'require':true},
        'company_addr_fam':{type:String, 'require':true},
    },
    personal:{
        'uid':{type:String, 'require':true},
        'givenname':{type:String, 'require':true},
        'surname':{type:String, 'require':true},
        'phone':{type:String, 'require':true},
        'dob':{type:String, 'require':true},
        'email':{type:String, 'require':true},
        'licsence_no':{type:String, 'require':true},
        'certificate_no':{type:String, 'require':true},
    },

}

for(let m in models){
	mongoose.model(m, new mongoose.Schema(models[m]))
}

module.exports = {
	getModel:function(name){
		return mongoose.model(name)
	}
}